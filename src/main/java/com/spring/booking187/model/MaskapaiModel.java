package com.spring.booking187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MASKAPAI")
public class MaskapaiModel {

	private String kodeMaskapai;
	private String namaMaskapai;
	private String ruteMaskapai;
	
	@Id
	@Column(name = "KD_MASKAPAI")
	public String getKodeMaskapai() {
		return kodeMaskapai;
	}

	public void setKodeMaskapai(String kodeMaskapai) {
		this.kodeMaskapai = kodeMaskapai;
	}

	@Column(name = "NM_MASKAPAI")
	public String getNamaMaskapai() {
		return namaMaskapai;
	}

	public void setNamaMaskapai(String namaMaskapai) {
		this.namaMaskapai = namaMaskapai;
	}

	@Column(name = "RUTE_MASKAPAI")
	public String getRuteMaskapai() {
		return ruteMaskapai;
	}

	public void setRuteMaskapai(String ruteMaskapai) {
		this.ruteMaskapai = ruteMaskapai;
	}

}
