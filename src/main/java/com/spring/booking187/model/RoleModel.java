package com.spring.booking187.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_ROLE")
public class RoleModel {
	
	private Integer idRole;
	private String kodeRole;
	private String namaRole;
	
	@Id
	@Column(name="ID_ROLE")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_ROLE") // utk buat nilai sequential
	@TableGenerator(name="M_ROLE", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_ROLE",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdRole() {
		return idRole;
	}
	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}
	
	@Column(name="KODE_ROLE")
	public String getKodeRole() {
		return kodeRole;
	}
	public void setKodeRole(String kodeRole) {
		this.kodeRole = kodeRole;
	}
	
	@Column(name="NAMA_ROLE")
	public String getNamaRole() {
		return namaRole;
	}
	public void setNamaRole(String namaRole) {
		this.namaRole = namaRole;
	}
	
	

}
