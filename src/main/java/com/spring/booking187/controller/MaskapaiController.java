package com.spring.booking187.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking187.model.MaskapaiModel;
import com.spring.booking187.service.MaskapaiService;

@Controller
public class MaskapaiController {

	@Autowired
	private MaskapaiService maskapaiService;

	// syntax untuk memanggil halaman maskapai
	@RequestMapping(value = "maskapai")
	public String maskapai() {
		String jsp = "maskapai/maskapai";
		return jsp;
	}

	// syntax untuk menampilkan list
	@RequestMapping(value = "maskapai/list")
	public String maskapaiList(Model model) {
		List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>();

		maskapaiModelList = this.maskapaiService.SearchAll();
		model.addAttribute("maskapaiModelList", maskapaiModelList);

		String jsp = "maskapai/list";
		return jsp;
	}

	// syntax untuk memanggil halaman pop up tambah
		@RequestMapping(value = "maskapai/add")
		public String maskapaiAdd() {
			String jsp = "maskapai/add";
			return jsp;
		}
		
	// syntax untuk tambah data
	@RequestMapping(value = "maskapai/create")
	public String maskapaiCreate(HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		String namaMaskapai = request.getParameter("namaMaskapai");
		String ruteMaskapai = request.getParameter("ruteMaskapai");

		MaskapaiModel maskapaiModel = new MaskapaiModel();
		maskapaiModel.setKodeMaskapai(kodeMaskapai);
		maskapaiModel.setNamaMaskapai(namaMaskapai);
		maskapaiModel.setRuteMaskapai(ruteMaskapai);

		this.maskapaiService.create(maskapaiModel);

		String jsp = "maskapai/maskapai";
		return jsp;
	}

	// syntax untuk lihat detail
	@RequestMapping(value = "maskapai/detail")
	public String maskapaiDetail(Model model, HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		
		maskapaiModel = this.maskapaiService.searchKode(kodeMaskapai);
		model.addAttribute("maskapaiModel", maskapaiModel);

		String jsp = "maskapai/detail";
		return jsp;
	}

	// syntax untuk pop up remove
	@RequestMapping(value="maskapai/remove")
	public String maskapaiRemove(Model model, HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		
		maskapaiModel = this.maskapaiService.searchKode(kodeMaskapai);
		model.addAttribute("maskapaiModel", maskapaiModel);
		
		String jsp = "maskapai/remove";
		return jsp;
	}
	
	// syntax untuk delete data
	@RequestMapping(value="maskapai/delete")
	public String maskapaiDelete(Model model, HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		maskapaiModel = this.maskapaiService.searchKode(kodeMaskapai);
		
		this.maskapaiService.delete(maskapaiModel);
		
		String jsp = "maskapai/maskapai";
		return jsp;
	}
	
	// syntax untuk pop up edit
	@RequestMapping(value="maskapai/edit")
	public String maskapaiEdit(Model model, HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		
		maskapaiModel = this.maskapaiService.searchKode(kodeMaskapai);
		model.addAttribute("maskapaiModel", maskapaiModel);
		
		String jsp = "maskapai/edit";
		return jsp;
	}
	
	// syntax untuk update data
	@RequestMapping(value="maskapai/update")
	public String maskapaiUpdate(Model model, HttpServletRequest request) {
		String kodeMaskapai = request.getParameter("kodeMaskapai");
		String namaMaskapai = request.getParameter("namaMaskapai");
		String ruteMaskapai = request.getParameter("ruteMaskapai");
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		
		maskapaiModel = this.maskapaiService.searchKode(kodeMaskapai);
		maskapaiModel.setNamaMaskapai(namaMaskapai);
		maskapaiModel.setRuteMaskapai(ruteMaskapai);
		
		this.maskapaiService.update(maskapaiModel);
		
		String jsp = "maskapai/maskapai";
		return jsp;
	}
	
}
