package com.spring.booking187.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking187.dao.SequenceDao;

@Repository
public class SequenceDaoImpl implements SequenceDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Integer searchForIdMahasiswaSeq(String sequenceName) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		Integer idMahasiswaSequence = 0;
		idMahasiswaSequence = (Integer) session.createQuery(" Select s.sequenceValue from  SequenceModel s where s.sequenceName='"+sequenceName+"' ").getSingleResult();
		
		return idMahasiswaSequence;
	}

}
