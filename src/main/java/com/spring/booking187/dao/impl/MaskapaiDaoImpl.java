package com.spring.booking187.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking187.dao.MaskapaiDao;
import com.spring.booking187.model.MaskapaiModel;

@Repository
public class MaskapaiDaoImpl implements MaskapaiDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(maskapaiModel);
	}

	@Override
	public List<MaskapaiModel> SearchAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>();

		String query = " from MaskapaiModel ";
		String kondisi = "";

		maskapaiModelList = session.createQuery(query + kondisi).list();

		return maskapaiModelList;
	}

	@Override
	public MaskapaiModel searchKode(String kodeMaskapai) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		MaskapaiModel maskapaiModel = new MaskapaiModel();
		
		String query = "from MaskapaiModel";
		String kondisi = " where kodeMaskapai = '" + kodeMaskapai + "' ";
		
		maskapaiModel = (MaskapaiModel) session.createQuery(query + kondisi).uniqueResult();
		
		return maskapaiModel;
	}

	@Override
	public void update(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(maskapaiModel);
	}

	@Override
	public void delete(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(maskapaiModel);
	}

}
