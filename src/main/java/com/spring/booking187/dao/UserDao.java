package com.spring.booking187.dao;

import com.spring.booking187.model.UserModel;

public interface UserDao {

	public UserModel searchUsernamePassword(String username, String password);
}
