package com.spring.booking187.dao;

import java.util.List;

import com.spring.booking187.model.MaskapaiModel;

public interface MaskapaiDao {

	public void create(MaskapaiModel maskapaiModel);
	
	public List<MaskapaiModel> SearchAll();
	
	public MaskapaiModel searchKode(String kodeMaskapai);
	
	public void update(MaskapaiModel maskapaiModel);

	public void delete(MaskapaiModel maskapaiModel);
}
