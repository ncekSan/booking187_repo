package com.spring.booking187.service;

import java.util.List;

import com.spring.booking187.model.MaskapaiModel;

public interface MaskapaiService {

	public void create(MaskapaiModel maskapaiModel);
	
	public List<MaskapaiModel> SearchAll();

	public MaskapaiModel searchKode(String kodeMaskapai);
	
	public void update(MaskapaiModel maskapaiModel);

	public void delete(MaskapaiModel maskapaiModel);
}
