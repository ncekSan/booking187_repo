package com.spring.booking187.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking187.dao.MaskapaiDao;
import com.spring.booking187.model.MaskapaiModel;
import com.spring.booking187.service.MaskapaiService;

@Service
@Transactional
public class MaskapaiServiceImpl implements MaskapaiService {
	@Autowired
	private MaskapaiDao maskapaiDao;

	@Override
	public void create(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.create(maskapaiModel);
		
	}

	@Override
	public List<MaskapaiModel> SearchAll() {
		// TODO Auto-generated method stub
		return this.maskapaiDao.SearchAll();
	}

	@Override
	public MaskapaiModel searchKode(String kodeMaskapai) {
		// TODO Auto-generated method stub
		return this.maskapaiDao.searchKode(kodeMaskapai);
	}

	@Override
	public void update(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.update(maskapaiModel);
	}

	@Override
	public void delete(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.delete(maskapaiModel);
	}
	
}
