package com.spring.booking187.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking187.dao.UserDao;
import com.spring.booking187.model.UserModel;
import com.spring.booking187.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	public UserModel searchUsernamePassword(String username, String password) {
		// TODO Auto-generated method stub
		return this.userDao.searchUsernamePassword(username, password);
	}

}
