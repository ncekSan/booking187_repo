package com.spring.booking187.service;

import com.spring.booking187.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
}
