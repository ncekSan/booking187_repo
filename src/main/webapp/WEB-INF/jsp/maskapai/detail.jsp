<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Lihat Maskapai</h1>
	<form action="#" method="get" id="form-maskapai-detail">
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai" value = "${maskapaiModel.kodeMaskapai}"/></td>
			</tr>
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai"name="namaMaskapai" value = "${maskapaiModel.namaMaskapai}"/></td>
			</tr>
			<tr>
				<td>Rute</td>
				<td><input type="text" id="ruteMaskapai" name="ruteMaskapai" value = "${maskapaiModel.ruteMaskapai}"/></td>
			</tr>
		</table>
	</form>
</div>

<script type = "text/javascript">

</script>