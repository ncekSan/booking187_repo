<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Edit Maskapai</h1>

	<form action="#" method="get" id="form-maskapai-edit">
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai" value = "${maskapaiModel.kodeMaskapai}"/></td>
			</tr>
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai"name="namaMaskapai" value = "${maskapaiModel.namaMaskapai}"/></td>
			</tr>
			<tr>
				<td>Rute</td>
				<td>
					<select id="ruteMaskapai" name="ruteMaskapai" value = "${maskapaiModel.ruteMaskapai}">
						<option>Domestik</option>
						<option>Internasional</option>
						<option>Keduanya</option>
					</select>
				</td>
			</tr>
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
function validasi() {
	var kodeMaskapai = document.getElementById("kodeMaskapai");
	if (kodeMaskapai.value == "") {
		kodeMaskapai.style.borderColor = "red";
	} else {
	}
	
	var namaMaskapai = document.getElementById("namaMaskapai");
	if (namaMaskapai.value == "") {
		namaMaskapai.style.borderColor = "red";
	} else {
	}
	
	var ruteMaskapai = document.getElementById("ruteMaskapai");
	if (ruteMaskapai.value == "") {
		ruteMaskapai.style.borderColor = "red";
	} else {
	}
	
	var kepemilikan = document.getElementById("kepemilikan");
	if (kepemilikan.value == "") {
		kepemilikan.style.borderColor = "red";
	} else {
	}
	
	var keterangan = document.getElementById("keterangan");
	if (keterangan.value == "") {
		keterangan.style.borderColor = "red";
	} else {
	}
}
</script>