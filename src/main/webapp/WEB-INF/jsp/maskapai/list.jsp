<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${maskapaiModelList}" var="maskapaiModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${maskapaiModel.kodeMaskapai}</td>
		<td>${maskapaiModel.namaMaskapai}</td>
		<td>${maskapaiModel.ruteMaskapai}</td>
		<td>
			<button type="button" class="btn btn-success" value="${maskapaiModel.kodeMaskapai}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${maskapaiModel.kodeMaskapai}" id="btn-delete">Hapus</button>
			<button type="button" class="btn btn-warning" value="${maskapaiModel.kodeMaskapai}" id="btn-detail">Lihat</button>
		</td>
	</tr>
</c:forEach>