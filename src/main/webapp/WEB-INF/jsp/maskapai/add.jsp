<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<div class="form-horizontal">
	<h1>Tambah Maskapai</h1>
	
	<form action="#" method="get" id="form-maskapai-add">
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai"/></td>
			</tr>
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai" name="namaMaskapai"/></td>
			</tr>
			<tr>
				<td>Rute</td>
				<td>
					<select id="ruteMaskapai" name="ruteMaskapai">
						<option>Domestik</option>
						<option>Internasional</option>
						<option>Keduanya</option>
					</select>
				</td>
			</tr>
			<tr>
				<td id="kepemilikan">Kepemilikan</td>
				<td>
					<input type="radio" id="kepemilikan1" name="kepemilikan1"/>Lokal
					<input type="radio" id="kepemilikan2" name="kepemilikan2"/>Asing
				</td>
			</tr>	
			<tr>
				<td>Keterangan</td>
				<td><textarea rows="3" cols="25" id="keterangan" name="keterangan"></textarea> </td>
			</tr>	
			<tr>
				<td>Fee Agen</td>
				<td>
					<p>*...</p>
				</td>
			</tr>	
		</table>
		<button type="submit" onclick="validasi();">Simpan</button> 
	</form>
</div>

<script type = "text/javascript">
	function validasi() {
		var kodeMaskapai = document.getElementById("kodeMaskapai");
		if (kodeMaskapai.value == "") {
			kodeMaskapai.style.borderColor = "red";
		} else {
		}
		
		var namaMaskapai = document.getElementById("namaMaskapai");
		if (namaMaskapai.value == "") {
			namaMaskapai.style.borderColor = "red";
		} else {
		}
		
		var ruteMaskapai = document.getElementById("ruteMaskapai");
		if (ruteMaskapai.value == "") {
			ruteMaskapai.style.borderColor = "red";
		} else {
		}
		
		var kepemilikan = document.getElementById("kepemilikan");
		if (kepemilikan.value == "") {
			kepemilikan.style.borderColor = "red";
		} else {
		}
		
		var keterangan = document.getElementById("keterangan");
		if (keterangan.value == "") {
			keterangan.style.borderColor = "red";
		} else {
		}
	}
</script>