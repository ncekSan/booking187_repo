<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Maskapai</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah
		Maskapai</button>

	<div>
		<table class="table" id="tbl-maskapai">
			<tr>
				<td>No</td>
				<td>Kode Maskapai</td>
				<td>Nama Maskapai</td>
				<td>Rute</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-maskapai">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Maskapai</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	loadListMaskapai();

	function loadListMaskapai() {
		$.ajax({
			url : 'maskapai/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-maskapai').html(result);
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

// ---------------------------------------------------------------------------------------------------- //	
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'maskapai/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit', '#form-maskapai-add', function() {
			$.ajax({
				url : 'maskapai/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {

					$('#modal-input').modal('hide');
					alert("data maskapai telah ditambah!");
					loadListMaskapai();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk popup lihat
		$('#list-maskapai').on('click', '#btn-detail', function() {
			var kodeMaskapai = $(this).val();
			$.ajax({
				url : 'maskapai/detail.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeMaskapai : kodeMaskapai
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup lihat
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk popup edit
		$('#list-maskapai').on('click', '#btn-edit', function() {
			var kodeMaskapai = $(this).val();
			$.ajax({
				url : 'maskapai/edit.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeMaskapai : kodeMaskapai
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup edit
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk update
		$('#modal-input').on('submit', '#form-maskapai-edit', function() {

			$.ajax({
				url : 'maskapai/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah diubah!");
					loadListMaskapai();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk popup hapus
		$('#list-maskapai').on('click', '#btn-delete', function() {
			var kodeMaskapai = $(this).val();
			$.ajax({
				url : 'maskapai/remove.html',
				type : 'get',
				dataType : 'html',
				data : {
					kodeMaskapai : kodeMaskapai
				},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup hapus
// ---------------------------------------------------------------------------------------------------- //	

		//fungsi ajax untuk delete
		$('#modal-input').on('submit', '#form-maskapai-delete', function() {

			$.ajax({
				url : 'maskapai/delete.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah dihapus!");
					loadListMaskapai();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
// ---------------------------------------------------------------------------------------------------- //	

	});
	//akhir dokumen ready
</script>